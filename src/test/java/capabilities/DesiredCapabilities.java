package capabilities;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;


public class DesiredCapabilities {

    public static Capabilities getCapabilities(Browser browser) {
        MutableCapabilities capabilities;
        Proxy proxy = new Proxy()
                .setProxyAutoconfigUrl("http://wpadvrn.t-systems.ru/wpad.dat");

        switch (browser) {
            case CHROME: {
                capabilities = new ChromeOptions()
                .addArguments("--ignore-certificate-errors")
                .addArguments("--start-maximized")
                .setProxy(proxy);
                break;
            }

            case FIREFOX: {

                capabilities = new FirefoxOptions()
                        .addPreference("app.update.auto", false)
                        .addPreference("app.update.enabled", false)
                        .addPreference("network.proxy.no_proxies_on", "localhost")
                        .setProxy(proxy);
                break;
            }

            default:
                throw new IllegalArgumentException("Invalid browser: " + browser);
        }

        capabilities.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
        capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        return capabilities;
    }
}