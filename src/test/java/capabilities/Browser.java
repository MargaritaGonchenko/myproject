package capabilities;

public enum Browser {
    FIREFOX("firefox"), CHROME("chrome");

    private String browser;

    Browser(String browser) {
        this.browser = browser;
    }

    @Override
    public String toString() {
        return browser;
    }
}
