import capabilities.Browser;
import capabilities.DesiredCapabilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import ru.stqa.selenium.factory.WebDriverPool;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class TestBase {

    WebDriver driver;

    @BeforeMethod
    public void initData() {

        File file = new File("src/test/resources/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

        driver = WebDriverPool.DEFAULT.getDriver(DesiredCapabilities.getCapabilities(Browser.CHROME));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://gitlab.com/users/sign_in");
        assertTrue(driver.findElement(By.tagName("h1")).getText().contains("GitLab.com"), "GitLab page is not opened");
    }

    @AfterMethod
    public void stop() {
        WebDriverPool.DEFAULT.dismissAll();
    }

    public void login() {
        WebElement name = driver.findElement(By.id("user_login"));
        name.clear();
        name.sendKeys("MargaritaGonchenko");

        WebElement password = driver.findElement(By.id("user_password"));
        password.clear();
        password.sendKeys("Trololo007");

        driver.findElement(By.name("commit")).click();
    }
}
