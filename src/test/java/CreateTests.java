import net.bytebuddy.utility.RandomString;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class CreateTests extends TestBase {

    @Test
    public void createNewProject() {

        login();
        driver.findElement(By.id("js-onboarding-new-project-link")).click();
        driver.findElement(By.xpath("//a[contains(text(), 'New project')]")).click();
        WebElement projectName = driver.findElement(By.id("project_name"));
        projectName.clear();
        projectName.sendKeys(new RandomString().nextString());

        driver.findElement(By.name("commit")).click();
        assertTrue(driver.findElement(By.id("content-body")).getText().contains("successfully created"), "Expected message is not present");
    }

    @Test
    public void createNewGroup() {

        login();
        driver.findElement(By.id("js-onboarding-new-project-link")).click();
        driver.findElement(By.cssSelector("[href = '/groups/new']")).click();

        WebElement groupName = driver.findElement(By.id("group_name"));
        groupName.clear();
        groupName.sendKeys(new RandomString().nextString());

        driver.findElement(By.name("commit")).click();
        assertTrue(driver.findElement(By.id("content-body")).getText().contains("successfully created"), "Expected message is not present");
    }

}

